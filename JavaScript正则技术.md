# 一、JavaScript正则引入
---
## [学习资料下载](http://pan.baidu.com/s/1eRTWev4)
---

## ①-正则引入

今天我们学习的技术是正则表达式，在说正则表达式之前，我们需要知道的，正则表达式是一门对字符串处理的技术（最早引入是神学网络学里面---引入Unix操作系统---Linux操作 sed命令 ---perl语言（正则支持最好的语言）百度百科），很多编程语言对正则都有支持（例如Perl、Java、php、JavaScript....）。今天我们学习的是JavaScript里面的正则表达式技术。一般正则也叫火星文。

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-9363e09bef422958.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


**问题引入：大部分人对正则还是不是很清楚，一般对传统字符串处理方式很清晰。**

1. 在学习正则表达式之前，先做一个小案例来对比传统的字符串操作和正则对字符串操作的不同之处
2. 案例：查找一个字符串里面所有的整数
a)var string = ‘12absdfdsf45sdafdsaf56’;  // [12, 45, 56]
3. 完成这个案例之前的话，我们先要补充一个知识，字符串之间是可以比较的，按照字符串的ascii码值比较的( 'a' < 'b' )[ 97 < 98]

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-1ba7ae5081a92dad.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


4.那么我们如何判断一个字符是否是数字类型的字符呢？('0'-'9') 

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-a1172d3e292e3142.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


5.可以判断这个字符是否在'0'-'9'这个数字类型字符范围之内即可

传统字符串处理方式
代码：

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-bc95801d73add6c0.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

效果：

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-e2e266394629e28a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


正则处理方式
代码：

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-fa05c23811cfecd8.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

效果：


![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-9982ce2c57eda419.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

通过上面的简单的引入，发现使用正则在处理字符串方面还是有其独特的优势。


## ②-什么是正则表达式？
看一下场景：

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-b8c64f29e9ac166e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

对于人类来说，基于日常的生活经验，我们可以很方便的看出某些字符的规律。但是对于计算机来说，它是很笨的，它并不能很好的理解人类所理解规则。但是计算笨不假，只有有电，干活还是很快（1G  42亿指令）所有人类喜欢奴隶计算帮助我们人类做事情。所有可以使用一定编程语言来帮我们去处理事情。并且这里学习是正则，所有我们得学习正则的编程语言来处理日常生活中常见的字符串。

####正则定义：
  正则其本质就是一个字符串规则（这个规则得满足一定的语法），然后通过这个规则可以非常方便的帮我们去处理字符串，找出海量字符串里面满足我们定义的规则的字符。正则其底层是一个对象。

一般使用的场景主要是：前台的验证、正则处理满足条件的字符串（网络爬虫 Python编程）数据采集（大宗点评：商家评论 抓取） curl函数库：模拟http请求。


## ③-正则语法
我们这次说的语法是js里面的语法，在其他语言里面不一定一样。
在js里面正则的语法一般有两种方式书写：
// 定界符，需要在定界符里面书写正则的表达式，千万主要不要在定界符的外层加引号
方式一：
var reg = /\d+/; 常用，简便 这种写法一般也被称为语法糖。
方式二：
var reg = new RegExp(‘\\d+’);

var arr = [];
var arr1 = new Array();

var json = {};
var json1 = new Object();


## ④-test方法
正则对象的第一个方法：test方法

案例1：测试某个字符串里面是否存在满足正则的规则的字符

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-02bd66e45499369a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

效果：

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-2992b97bafa16a47.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


总结：正则对象的test方法的返回值是一个boolean值，如果在处理的字符串里面能够找到满足正则规则的字符，返回true；否则返回false。比较多



## ⑤-字符串方法和正则对象的配合
一般来说正则对象单独作为一个对象去调用方法比较少，一般都是和字符串的方法配合使用，作为字符串方法的参数进行传递。

字符串串里面常见的方法：
1.substring 做字符串的截取 [start, end) 
2.search 做某个字符串搜索，返回值是从0开始对应的下标，找不到返回-1
3.replace 做字符串的替换，查询某个字符串里面是否存在满足条件的字符串，使用新的字符串进行替换
4.match 做查找的匹配，并且把匹配的结果以数组的方式返回
5.repeat 让某个字符串重复出现多少次
6.split 做字符串的分割，把某个字符串按照一定的分隔符分割成数组

### 字符串-match方法
案例1： 直接把字符作为参数传递给match方法

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-5826d2d7b835dd70.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

效果

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-d1f79b00b017f6c4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


案例2：把正则对象作为参数传递给match方法

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-b2c6839e9948e643.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

效果：

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-ff095a389b68ee69.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

特别注意：
元字符：
\d代表的含义被称为js正则里面的转移字符， d 代表就代表一个普通的字符d ，但是在前面加了一个 \ ，现在\d含义就变了，变成了一类字符的集合0-9之间的任何一个数字。

模式修饰符：
1. 在js的真这个里面match方法默认是找到满足条件的就停止，不再继续的向后查找，如果要让它向后继续查找，需要在正则表达式加上g，这个g被称为模式修饰符。主要是修饰正则表达式该如何去操作，代表的意思，不要停止，继续去字符串找到所有满足条件的字符。

2. 在js里面还有一个模式修饰符，i代表含义是ignore，不区分大小写

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-ec2f5b61dc666702.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

效果：


![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-c7dae079b9b67c2e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


量词：
1. + 在js正则里面被称为量词，用来修饰前面的正则规则出现的次数，+代表1次或者多次 多
2. * 代表的含义就是 0次或者以上，但是一般少用 手机号码 0*18620628729
3. ? 代表出现0次或者1次

案例3：在一个字符串里面查找所有的整数
代码：
![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-3de51875157a51a8.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

效果：


![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-0f877a11582bc3d5.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

特别注意：
1. \d 代表 0-9之间的任何一个数字，对于这种写法还有另外的一种方式表示， [0-9] 代表0-9之间一个数字
2. + 这种写法也存在另外的一种写法，{1,}代表出现最少一次，最多不限


### 字符串-replace方法

案例1：传统字符串方式字符串，替换里面满足条件的字符

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-143f888c820acdc0.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


效果：

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-22621bab38ea2af1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

注意：replace方法找到满足条件的字符进行替换后，里面停止，不再向后继续处理。


案例2：正则对象使用，替换里面满足条件的字符

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-7bdd94e9d01f0318.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


效果：

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-5f909ad9a2d60bab.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)



案例3：做一个敏感字替换，效果如下
![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-3382733bff00115d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

代码：
![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-21e357f75a01600e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

效果：

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-04cbdec8afae8fc0.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

优化：如果敏感字有几个长度，则替换的位置显示多少个*号。

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-5603c2af1ffa2364.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

效果：

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-a47ba0e291e77762.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

特别注意：
1.replace可以接受第二个参数，是一个回调函数，当使用正则去处理字符串的时候，如果能够找到满足条件的部分，回调函数里面会执行
2.replace第二个参数的返回值就是新的替换值；在js如果函数不存在返回值，返回undefined
3. replace回调函数存在参数，第一个参数就是满足正则的字符串，会被传递回调函数

案例4：把下面的中划线的命名变成小驼峰的方式
使用传统字符串方式实现：

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-5ce3bdf4463d487b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

效果：

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-153597cf0eb7205f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

正则实现：

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-c99ba497b0bce8f0.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

效果

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-6a0a55868b87408e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

特别注意：
1.在js正则里面的()被称为子项（分组）,从左往右数，第一个小括号被称为第一个子项，依次类推
2.在js的replace方法的第二个回调函数里面的第一个参数是满足整个正则的字符串，其后的参数就是满足子项的字符串

案例5：把某一个字符串里面出现次数最多的字符找出来
传统字符串方式处理：

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-92958572fa5ed1d7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

效果：


![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-fe609c91ed08e3dd.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

正则处理：

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-30f6f739a183c82a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

效果：

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-868ec0ac58a63b7a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

特别注意：
1. 正则表达式里面的小括号被称为子项，从左到右，依次称为第一个，第二个...；如果需要获取子项里面的内容获，可以使用\1 代表对第一个子项的引用 ，\2代表是对第二个引用。

扩展：数组去重

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-2ea09b3f0989f880.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

效果:

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-d585c51a91fc5850.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


# 二、正则字符类
1.什么是字符类？
答：就是js正则里面一类字符的集合，例如所有的小写字母。
[a-z]
所有的大写字母
[A-Z]
所有的数字
[0-9]
需要某一个部分的字符[&*]、[abcd]、[a-d]

例如常见的字符类也存在简写
[0-9]  \d
js里面所有的字符 0-9a-zA-Z_ 表示方法：\w 、[ [a-zA-Z0-9]

案例1：注册用户名检测
规则：
1.用户的首字母不能是数字
2.长度一般是4-8位长度
3.其他后面的部分是可以a-zA-Z0-9_中的字符

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-b6d4cf1fbdb4b311.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

效果

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-7350454a9713f9c2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

特别注意：
1.在字符类里面，如果在中括号里面加上^号之后，代表不取这个[]里面的字符
2.[]只代表其中的一个字符
3.^如果不是在[]里面使用，代表字符串的行首； $代表是行尾，一般在表单的验证的时候都要加上行首行尾。但是一般来说我们也很少自己去写表单验证，一般是要验证的插件，等会学习一个validate插件，可以帮我们做前台验证。


# 三、正则里面的量词
一般在js正则里面，有的需要修饰前面的字符出现的次数，这个时候我们可以使用量词来操作。
量词常见存在两种写法：
写法1：
a.+ 代表前面的字符出现最少一次，最多不限制 
b.* 代表最少出现0，最多不限
c.? 代表出现0或者1次 （手机号码）

写法2：
a.{1,} 代表出现最少1次
b.{3,6} 代表最少出现3，最多出现6次（最多）
c. {0,1} 代表最少0次，最多1次
d. {9} 代表正好要出现9次

案例1：手机号码验证（面试）
手机号码规则：
1.现在手机号码可能11或者12；例如：18620628729   018620628729，手机号码最前的0可以出现，也可以不出现
2.对于11手机号码，首位必须1
3.除开首位之后的第二位只能是  3 4 5 7（阿里）8
4.除开首位和第二位之后的位数可以0-9之间任何一个


![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-b3506e5492f05573.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

案例2：去除字符串里面的空格（笔试写一个正则）

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-102180aca4c033a4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

效果：


![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-9e273a5228c305d6.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

案例2-1：去除一个字符串中间的空格，但是不要求去除前后空格

```javascript
var string = " a cf   sdfadsfdsdf      sasd as asfs sa f    ";

	console.log( "(" + string + ")");
	//原始思路var reg = /^(\s*)([a-z\s]+)\s*$/g;
	// ([a-z\s]+)这部分会做贪婪匹配，把行尾之前的所有的空格全部匹配，所以要取消贪婪
	// 
	// 第一部分：^(\s*) 代表行首部分的空格
	// 第二部分：([a-z\s]+?) 代表中间是a-z或者空格，但是不要贪婪匹配
	// 第三部分：(\s*)$ 代表行尾空格
	var reg = /^(\s*)([a-z\s]+?)(\s*)$/g;

	var newString = string.replace(reg, function() {
		console.log( "(" + arguments[0] + ")");
		console.log( "(" + arguments[1] + ")");
		console.log( "(" + arguments[2] + ")");
		console.log( "(" + arguments[3] + ")");
		return arguments[1] + arguments[2].replace(/\s+/g,'') + arguments[3];
	});
	console.log("(" + newString + ")");
	console.log("(" + string.match(reg).toString() + ")");

```

案例3：QQ号验证
规则：
1.目前QQ号最少5位，最多10位
2.QQ号的首位不能为0
3.首位之后的其他为0-9中任何一个

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-16d3c94817d72bf7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


案例4：邮箱验证(主要是为了面试)
规则：
gogery@163.com
第一部分：目前的用户名只能[0-9a-zA-Z_] 出现的次数{3,10}
第二个部分：@
第三个部分：[a-z0-9] 域名 出现的次数{1,4}
第四个部分：. 但是需要特别注意 ，. 在js正则里面存在特殊的含义，也被称为元字符，代表除开换行之外的任何字符都可以。如果我们希望去除掉特殊，需要进行转义,前面加一个 \ ， \. 这个时候只代表普通的 . 
第五部分：根域 [a-z]， 出现{2,4}

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-6a420a2b9d64cc14.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)



# 四、正则中文的处理
一般在字符都存在一个编码的规则，对于中文的编码一般都是使用utf-8编码，并且js天生使用的就是utf-8对待字符串。并且utf-8的编码是存在一个范围的[\u4e00-\u9fa5]基本把所有常见中文都涵盖。

案例1：一般是在相亲网站里面的用户注册一般都需要首字母是中文。

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-ebeebc97587e7d57.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

一般来说我们作用户名检查的时候，都是需要注意用户名字符串里面不包含中文。

面试宝典：设计一个简单的算法，来计算相亲网站一个用户注册的时候，给他|她|它匹配一个缘分指数。（开放性的问题）
面试题：现在存在一个1kw人口城市，需要多少个理发师（合理）？思路
面试题：井盖为什么是圆的？人的手指不是一般长？
面试题：2个2T文件，里面数据是按行分割，找出里面重复的单元。（开放性思维）


# 五、正则里面的正向预言和反向预言
一般也叫作正向言宽和反向言宽。因为这个知识点很少很少。

## 正向语言


![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-aadc135eceee8694.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

效果

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-29345a11714b5f3f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


## 反向预言

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-1ae1ce809d505854.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

效果：

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-ccc4e771f5f96e5f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


## 补充：单词的边界
在js里面对于一个字符来说，空格，行首和行尾被称为边界，使用元字符\b表示。
案例补充：同一个字符串里面的单词的个数

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-da6a7c3b32b3cfa9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

效果：

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-787fd0485b7c1a65.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


## 千分符处理

**什么是千分符？**
> 将一个整数从右往左数，每三位增加一个逗号。一般在显示金钱的时候。形如这样 **12,345,678**

案例2：现在存在一个数字类型的字符，需要使用千分符的方式来处理这个字符串
`var string = '12345678';`
千分符：一般在显示金钱的时候，要使用（电商网站）
形如这样：12,345,678

### 传统字符串的方式处理

思路：
1. 由于千分符是3位一分割，所有我们去查看字符串的长度是否正好是3的倍数，那就可以那字符串的长度对3取摸【取余】，查看余数是多少 0 1 2
2. 如果是0则正好是3的倍数，则3为一分割
3. 如果不是0，先把多余部分的分割出来，剩下部分3为一分割


![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-f88b01656d6f5b3e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-1aa3ab349c3d9045.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


效果：

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-966fb2b30316c2aa.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


### 正则处理
> 正向预言和反向预言实现和边界

#### 正则处理方式一

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-972faea0db49c776.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

效果：

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-4b0fe1b11ec56fa3.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

####  正则处理方式二

![方式二](http://p1.bqimg.com/4851/a4586392c5fd9a09.png)

```javascript
	// 千分符处理思路：因为千分符是从右往左数，这样不好处理，则我们可以反着操作
	// 1. 先把字符串反转，使得可以每三位一数
	// 2. 但是需要注意的是，字符串正好是3的倍数，最后一位不能加,
	// 3. 处理完后在重新反转 
	var string = '12345678';
	var newString = string.split('').reverse().join('');
	// 正则编写
	// a. (\d{3}) 代表是3位一组的数据
	// b. (\d{3}(?!$)) 但是最后不能是行尾
	var reg = /((\d{3})(?!$))/g;
	newString = newString.replace(reg , "$1,");

	console.log( newString );
```

#### 正则处理方式三
![方式三](http://p1.bqimg.com/4851/200256411078f378.png)

```javascript
	// 千分符：千分符是从右往左数，是每三位一组。
	var string = '123456';
	// 思路：分两步处理 a. 第一部分处理一组，可能是1-3位 b. 第二部分正好是三的倍数
	// 1. 字符串的开始必须1位或者2位一组进行处理
	// 2. 剩下的部分是3的倍数，但是结尾不能加逗号
	var reg = /^(\d{1,3})((\d{3})+)$/;
	var newString = string.replace(reg, function($0, $1, $2) {
		// $0 代表正则匹配的内容 $1 代表第一个分组 $2代表第二个分组
		// 其中$1代表第一部分，则后面加上一个, 
		// 第二部分是3的倍数，但是最后$不要加,
		return $1 + ',' + $2.replace(/\d{3}(?!$)/g, function($0) {
			return $0 + ',';
		});
	}) 
	console.log( newString );
```


#### 正则处理方式四
![方式三](http://p1.bqimg.com/4851/0851b02f8d7bd04e.png)

```javascript
	// 千分符：千分符是从右往左数，是每三位一组。
	var string = '123456789';
	// 思考：
	// 1. 如何实现字符倒着数？
	// 	字符串长度 - 当前索引 == [字符串长度6]
	// 	字符6的当前索引是 5 ，则当前数数为： 6 - 5 = 1
	// 	字符5的当前索引是 4 ， 则当前数数为： 6 - 4 = 2
	// 2. 什么时候加 , ，当前数数为3的时候，则在前面加上 , 
	//    问题：如果正好是3的倍数，则起始位置会被误操作加上, 则我们可以转换思路
	// 3. 思路转换
	//   在当前数数为3的下一位后面加上 , 即可
	//  字符串长度 - 字符索引 -1 mod 3 == 0，则在后面加上一个 ,
	var reg = /\d(?!$)/g; // 最后的数字不要加上,
	var newString = string.replace(reg, function($0, $index){
		if((string.length - $index -1) % 3 == 0 ){
			return $0 + ",";
		}else{
			return $0;
		}
	});

	console.log( newString );
```

### 扩展：在php里面存在一个number_format函数可以实现千分符。

[学习地址](http://ask.sinsea.cn/?q-33.html)


# 六、扩展-validate插件使用

## ①-简介
![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-3c352600de833fb2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
学习地址：http://www.runoob.com/jquery/jquery-plugin-validate.html

展示效果：

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-cd8d2108eb2743ff.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

## ②-插件使用
1.把对应的插件下载到本地，放置网站根目录的资源目录里面
![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-cecff80637b50c51.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

2. 顺序引入的引入插件
![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-52f9058706e4ff19.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

3.为定义form的id属性，方便插件调用时候获取该DOM元素
![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-116fbb7676d61e94.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

4.js调用代码
![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-170410b578ec4925.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
效果：
![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-d8bb02b4e695b9ee.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
](http://upload-images.jianshu.io/upload_images/267368-da9e118a1606164b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


5.为提示信息加一个边框样式

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-367da6f4fef98537.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


在点击提交没有通过的通过的情况下，会在input后面增加一个class为error的label标签，只需要给label.error写一个样式。

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-ce901eba67bddf33.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

效果：

![Paste_Image.png](http://upload-images.jianshu.io/upload_images/267368-3445c86563d9a48b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)