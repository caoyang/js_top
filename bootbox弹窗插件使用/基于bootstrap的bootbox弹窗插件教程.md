# bootbox弹窗插件使用

## 简介和效果展示

### 简介
1. bootbox是什么？
> Bootboxjs是一个简单的js库，简单快捷帮你制作一个Bootstrap的弹出框效果。


2. 相关文档

> [官方网站](http://bootboxjs.com/)
> [API文档](http://bootboxjs.com/documentation.html)

### 效果演示

1. 自定义弹窗效果展示

![自定义弹窗效果](http://p1.bpimg.com/4851/b6f9c1b0b8ccc42e.jpg)

## 入门使用

1. 先引入css、js文件，需要注意的是该弹窗插件依赖bootstrap的css和js文件，以及jQuery.js
2. 查看API文档，调用alert、prompt、confirm、dialog等方法，完成弹窗效果
3. 效果

![演示效果](http://p1.bpimg.com/4851/ac0f13174a724d53.jpg)

## 相关函数

### 函数分类
> bootbox.js提供常用的三方法设计可以实现浏览器提供的JavaScript一些方法，例如alert、prompt、confirm。

+ bootbox.alert(message, callback)
+ bootbox.prompt(message, callback)
+ bootbox.confirm(message, callback)

### 测试

+ alert函数

```javascript
// 当点击弹出框的OK按钮后，callback函数会被调用
bootbox.alert("bootbox弹窗插件使用?", function() {
	console.log( 'alert callback' );
});
```

+ prompt函数

```javascript
// 当用户点击cancel按钮时候，result的值是null
bootbox.prompt("bootbox弹窗插件使用?", function(result) {
	if(result === null){
		console.log( result );
	}else{
		console.log(' ok confirm button. ');
	}
});
```

+ confrim函数

```javascript
// 当用户点击弹出框的OK按钮，result的值为true，反之为false
bootbox.confirm("Are you sure?", function(result) {
	console.log(result);
});
```


## 进阶使用

## 中文设置
> 由于bootbox插件默认的文字是英文，但我们项目一般使用的中文，所以可以设置bootbox的文字

```javascript
// 设置中文
bootbox.setLocale('zh_CN');

bootbox.prompt("bootbox弹窗插件使用?", function(result) {
  console.log( result );
});
```

### dialog函数
> 主要用于用户自定义弹窗效果

```javascript
bootbox.setLocale('zh_CN');
// 自定义弹窗
bootbox.dialog({
  // dialog的主体内容
  message: "<h2>欢迎大家使用bootbox插件！！！<a href='http://ask.sinsea.cn/' target='_blank'>php问答系统提供</a></h2>",
   
  // dialog的标题
  title: "<h3>bootbox插件</h3>",
   
  // 退出dialog时的回调函数，包括用户使用ESC键及点击关闭
  onEscape: function() {},
   
  // 是否显示此dialog，默认true
  show: true,
   
  // 是否显示body的遮罩，默认true
  backdrop: true,
   
  // 是否显示关闭按钮，默认true
  closeButton: true,
   
  // 是否动画弹出dialog，IE10以下版本不支持
  animate: true,
   
  // dialog的类名
  className: "my-dialog",
   
  // dialog底端按钮配置
  buttons: {
     
    // 其中一个按钮配置
    success: {   
      // 按钮显示的名称
      label: "取消!",
       
      // 按钮的类名
      className: "btn-success",
       
      // 点击按钮时的回调函数
      callback: function() {}
    },

    // 另一个按钮配置
    "确认!": {
      className: "btn-danger",
      callback: function() {}
    },
    // 另一个按钮配置
    "考虑中...": {
      className: "btn-danger",
      callback: function() {}
    }
    // 按钮可以配置多个
  }
});
```



