# 专题一：JavaScript中this指向问题

本次我们研究一下JavaScript里面的this的指向的问题，主要是研究函数里面this，但是不仅仅只有函数里面存在this，
全局内也是存在this的，代表的是window。但是我们主要研究的是函数里面this？

## 那么函数里面的this的代表的是什么？

`答：函数里面的this代表的是当前行为的执行的主体。同时在JavaScript里面当一个函数执行的时候存在一个执行环境，一般叫做context（上下文）。`


例如：现在有一个吃饭的行为，那么里面的this代表是什么？
```javascript
funtion 吃饭(){
	console.log( this ) //?
}
```
分析：

吃饭的行为不能自己发生，需要一个执行主体，例如小明.吃饭()；这个时候函数里面的this代表
的就是小明；同时小明可以在家里吃饭，也可以在学校吃饭。
那么：家、学校就是上下文，this就是小明，行为就是吃饭。小明可以在任何地方完成吃饭的行为。并且小明在吃饭的行为和在哪里是没有关系的。

`总结：在JavaScript里面this在哪个环境执行和在哪里定义是没有关系的。`

## 如何区分JavaScript里面的this呢？

主要存在如下的三条规则：

+ 函数执行，首先看函数前面是否存在点，如果存在点，那么前面是谁，this就是谁；如果没有点，则就是window
+ 匿名函数自执行里面的this永远是window，因为前面不可能存在点
+ 当给某个DOM的某一事件绑定方法，当事件触发后，回调函数里面的this代表当前DOM元素。

### 案例演示
a. 函数执行，首先看函数前面是否存在点，如果存在点，那么点前面是谁，this就是谁；如果没有点，则就是window
案例一：
```javascript
funtion fn(){
	console.log( this ); //当fn执行的时候，this代表的是 ? 
}
fn();
```

案例二：
```javascript
var obj = {fn:fn};
obj.fn();
```

案例三：
```JavaScript
funtion sum(){
	fn();
}
// fn里面的this是谁？
sum();
```

案例四：
```javascript
var obj1 = {sum:funtion(){
	fn();
}};

obj1.sum();// fn里面的this是谁？sum里面的this是谁？
```

b. 匿名函数自执行里面的this永远是window，因为前面不可能存在点
案例一：
```javascript
(function(){
    console.log(this); // this ?
})();
```

c. 当给某个DOM的某一事件绑定方法，当事件触发后，回调函数里面的this代表当前DOM元素。

案例一：
```javscript
document.getElementById('div1').onclick = fn; // this ?
```

案例二：
```javascript
document.getElementById('div1').onclick = funtion(){
	fn(); // this ?
}
```


# 案例综合分析
```javascript
var number = 20;
var obj = {
	number : 30,
	fn : (function(number){
		this.number *= 3;
		number += 15;
		var number = 45;
		return function(){
			this.number *= 4;
			number += 20;
			console.log(number);
		};
	})(number)
};

var fn = obj.fn;
fn();
obj.fn();
console.log(window.number, obj.number);
```

#[资料下载](http://pan.baidu.com/s/1ge57cP1)