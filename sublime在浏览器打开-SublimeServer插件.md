# SublimeServer插件

## 简介
>  如果大家有使用过Sublime Text编辑器，那么在进行HTML和Javascript开发的时候可能需要在浏览器里面显示我们开发的页面，帮你完成前端的测试。这个时候我们的SublimeServer 插件可以闪亮登场了。使用了该插件后，就不在需要单独的启动nginx或者Apache这样的重型服务器，就可以完成HTML和Javascript在服务器的环境下的调试。SublimeServer会启动一个轻量级的，静态的WEB服务器，让你在文本编辑器中直接启动服务器，并进行测试。


## 安装和使用

1. 使用 **ctrl + shif + p**启动sublime的命令行

2. 在命令行输入 **Install Package** 
3. 输入SublimeServer找到Sublime Text的服务器插件，回车直接安装即可
4. 安装完成后，点击工具(Tools)选项，就可以看到SublimeServer工具了，然后点击Settings，查看SublimeServer的基本配置，这里可以修改服务器端口，文件扩展名等
5. 查看好设定后，点击Start SublimeServer，就可以启动服务器了
6. 注意：SublimeServer要求代码文件夹，要添加到Sublime Text的项目里面