#一、markdown简明教程-文本格式化
---

##1、水平线，只需要使用三个中划线 ---
演示：

---

##2、斜体使用    *斜体内容* 或者 _斜体内容_
演示：

*我是斜体* _我也是斜体_

##3、粗体使用    **强调内容** _强调内容__
演示：

**强调内容**

__强调内容__

##4、引用格式
> [我爱开发网](http://www.woaikaifa.com '链接提示文本')  
> [PHP问答系统](http://ask.sinsea.cn '问答系统')

##5、删除线使用  ~~删除内容~~
演示：

~~删除内容~~

---

#二、markdown简明教程-代码格式化

##行内代码
演示：
`console.log(1)`

##块状代码
演示
```javascript
function fn(){
  var name = 'http://www.woaikaifa.com';
  console.log( name ); 
}
```
---

#三、markdown简明教程-无须列表和有序列表

##无序列表
演示：

- 序列一
- 序列二

## 有序列表
1. 序列一
2. 序列二

##嵌套列表-只需要在子级使用tab键入即可
演示一：

- 序列一
  - 子级序列一
  - 子级序列二
- 序列二

演示二：
1. 序列一
  1. 子级序列一
  2. 子级序列二
2. 序列二
  - 子级序列三
  - 子级序列四
3. 多段列表【只需要在新起的行使用tab缩进即可】
  我是段落一
  我是段落二
  我是段落三

---
#四、markdown简明教程-链接和图片
##行内式链接
演示:
[我爱开发网](http://www.woaikaifa.com/ '我爱开发网')

##行内式图片
演示：
![我爱开发网logo](http://www.woaikaifa.com/favicon.ico 'logo')

---
# 五、markdown简明教程-脚注
- 先定义
[^name]:曹阳
[^blog]:http://www.woaikaifa.com


- 使用
曹阳[^name]是谁？他的个人[网站](http://www.woaikaifa.com)[^blog]是**我爱开发网**

---

# 六、表格
|姓名|性别|年龄|
|:----|:----:|----:|
|曹阳|男|12|
|mark|男|34|
|Andy|男|34|
|ruby|女|21|

> 备注：
> 表头使用四个----即可，同时在中线右侧加冒号代表要右排列
> 同时在中线两侧加冒号代表要居中排列

# 参考资源
> [webstorm安装markdown](http://www.cnblogs.com/joqk/p/3864736.html)
> [简明教程](http://www.aichengxu.com/other/1500644.htm)
> [在线markdown](https://www.zybuluo.com/mdeditor?)





