# PHPstorm开发工具使用
    
## 简介
> PhpStorm是一款由JetBrains公司开发推出的商业PHP集成开发工具，软件不仅包含了webstorm的全部功能，还拥有php、javascript、HTML/css等编辑器，能为开发者提供最便捷最智能的代码编写，同时其内置自动生成phpdoc注释、集成分析器、可视化调试等功能，能轻松解决智能代码补全、快速导航以及即时错误检查等问题，是各类大型php开发项目必备的优秀平台。
    PhpStorm 10改进了PHP 7支持，提供phpStorm插件，并且修复和改进了PHP、Web、IntelliJ等平台的其他bug。虽然官方不支持中文版本，但本次带来的汉化补丁可让大家体验PhpStorm10中文汉化版。 
    
## 功能特点

1. 智能PHP编辑器 
    - 	PHP代码补全
    - 	智能的重复编码检测器 
    - 	PHP重构 
    - 	支持Smarty和PHPDoc 
    - 	支持多语言混合 
2. Java Script 编辑器 
    -   基于DOM/指定浏览器完成 
    -   代码导航和用法查找 
    -   JavaScript 重构 
    -   JavaScript调试器 
3. HTML/CSS编辑器 
    - 	支持HTML5 
    - 	支持Zen编码 
    - 	检验和快速修正 
    - 	显示应用的外观 
    - 	提取嵌入样式 
4. 轻量级IDE 
    - 	易于安装 
    - 	可在Windows, Mac OS X, Linux上运行
    - 	项目配置简单-可在任意地方打开代码开始工作 
    - 	性能优先 
5. 智能的环境 
    - 	可视化PhP单元测试运行期 
    - 	VCS支持SVN、Git、Mercurial等 
    - 	支持FTP和远程文件同步 
    - 	可记录本地修改 
    - 	可视化调试 
    - 	无需任何配置的调试器 
    - 	支持在PHP，JS，HTML中设置断点 
    - 	观察变量，窗口 
    - 	批量代码分析 
    - 	集成分析器 
    
## 下载和安装
> 下载地址 [百度云网盘地址](http://pan.baidu.com/s/1pLLP5Xp)
    
1. 解压并打开文件，双击【PhpStorm-10.0.3.exe】开始安装。
2. 一路默认安装即可。
3. 勾选【创建桌面快捷方式】和【选择关联文件选项（根据自己实际需要勾选）】。
4. 安装完成，勾选运行并点击【finish】，即可运行软件。
5. 开启软件后，会弹出的注册栏目，点击【ok】, 再选择【 Activation code 】前往 http://idea.lanyus.com/  这个网站获取激活码，然后复制下来，  点击【OK】即可快速激活软件。【备注：文末附带激活码】 
7. 安装完成后软件还是英文界面，先不要关闭。
8. 下面我们开始汉化的步骤，在运行的phpstorm10菜单上依次选择File -> Settings -> Appearance&Behavior -> Appearance -> 选中Override default fonts by(not recommended)->选择任何一种中文字体（后面需要，防止出现乱码），文件设置好以后即可关闭软件。
9. 打开下载文件中PhpStorm10汉化包，最后将PhpStorm10汉化包中的【resources_cn.jar】文件复制回C:\Program Files (x86)\JetBrains\PhpStorm 10.0.3\lib目录(注意这是默认的安装目录)，完毕！ 
11运行桌面的快捷方式，便是phpstorm 10.0.3破解版的中文界面了。

## 常见FQ？

1. 如何在PHPstorm里面设置窗口主体或者字体等信息？
2. 如何设置PHPstorm里面代码的字体样式？
3. 如何在PHPstorm一个窗口里面打开多个项目？
4. 如何将PHPstorm项目的代码上传到线上服务器？
5. 如何显示代码里面的行号？
6. 如何折行显示代码？


## 常见快捷键大全

- alt  +  1 快速折叠左侧菜单
- shift + shift 快速查找文件
- ctrl + n 快速查找类名
- ctrl + shift + n 快速查找文件名
- ctrl + e 打开最近打开的文件
- ctrl + r 在当前文件查找并替换
- alt + shift + c  查找修改的文件，最近变更历史
- alt + shift + l  格式化代码
- ctrl + d 复制当前行
- alt + shift + ↑|↓ 快速的移动光标
- ctrl + shif + x 快速开启命令行
- ctrl + g 快速跳转到指定行
- shift + enter 快速开启下一行
- ctrl + delete 开始删除光标到行尾
- ctrl + shift + j 将多行快速变成一行
- ctrl + b 查看函数定义
- ctrl + shift + alt + n 查看函数定义
- ctrl + alt + s 打开系统设置
    
## 相关链接资料

[phpstorm简介](http://www.jb51.net/softs/430886.html)

[破解安装](http://www.42xz.com/phpstorm-10-0-3/7218.html)

[一个窗口打开多个项目](http://unun.in/php/135.html)

[常见资料](http://unun.in/php/135.html)

[phpstorm删除项目](http://unun.in/php/10.html)

[phpstorm代码片段](http://blog.csdn.net/qq_15766181/article/details/48176845)

[代码片段](http://confluence.jetbrains.com/display/PhpStorm/Live+Templates+(Snippets)+in+PhpStorm)

[常见设置](http://blog.csdn.net/fenglailea/article/details/53350080)



## 注册码
> CNEKJPQZEX-eyJsaWNlbnNlSWQiOiJDTkVLSlBRWkVYIiwibGljZW5zZWVOYW1lIjoibGFuIHl1IiwiYXNzaWduZWVOYW1lIjoiIiwiYXNzaWduZWVFbWFpbCI6IiIsImxpY2Vuc2VSZXN0cmljdGlvbiI6IkZvciBlZHVjYXRpb25hbCB1c2Ugb25seSIsImNoZWNrQ29uY3VycmVudFVzZSI6ZmFsc2UsInByb2R1Y3RzIjpbeyJjb2RlIjoiQUMiLCJwYWlkVXBUbyI6IjIwMTgtMDEtMzAifSx7ImNvZGUiOiJETSIsInBhaWRVcFRvIjoiMjAxOC0wMS0zMCJ9LHsiY29kZSI6IklJIiwicGFpZFVwVG8iOiIyMDE4LTAxLTMwIn0seyJjb2RlIjoiUlMwIiwicGFpZFVwVG8iOiIyMDE4LTAxLTMwIn0seyJjb2RlIjoiV1MiLCJwYWlkVXBUbyI6IjIwMTgtMDEtMzAifSx7ImNvZGUiOiJEUE4iLCJwYWlkVXBUbyI6IjIwMTgtMDEtMzAifSx7ImNvZGUiOiJSQyIsInBhaWRVcFRvIjoiMjAxOC0wMS0zMCJ9LHsiY29kZSI6IlBTIiwicGFpZFVwVG8iOiIyMDE4LTAxLTMwIn0seyJjb2RlIjoiREMiLCJwYWlkVXBUbyI6IjIwMTgtMDEtMzAifSx7ImNvZGUiOiJEQiIsInBhaWRVcFRvIjoiMjAxOC0wMS0zMCJ9LHsiY29kZSI6IlJNIiwicGFpZFVwVG8iOiIyMDE4LTAxLTMwIn0seyJjb2RlIjoiUEMiLCJwYWlkVXBUbyI6IjIwMTgtMDEtMzAifSx7ImNvZGUiOiJDTCIsInBhaWRVcFRvIjoiMjAxOC0wMS0zMCJ9XSwiaGFzaCI6IjUxOTU1OTMvMCIsImdyYWNlUGVyaW9kRGF5cyI6MCwiYXV0b1Byb2xvbmdhdGVkIjpmYWxzZSwiaXNBdXRvUHJvbG9uZ2F0ZWQiOmZhbHNlfQ==-QOxwjWvRwJz6vo6J6adC3CJ4ukQHosbPYZ94URUVFna/Rbew8xK/M5gP3kAaPh6ZDveFdtMR1UBoumq3eCwXtXM3U3ls5noB4LIr+QplVlCj2pK5uNq7g/feyNyQcHpSXtvhIOnXDBLOecB05DOsxzm0p7ulGGJoAInmHeb9mc0eYjqc4RPpUQfh6HSYBnvEnKMlLF5bz4KEtzmsvvgA55CwzwQ3gRitm5Q/wUT7AQCBdjmBfNUjKVQL6TSjSDPp56FUdEs4Aab8LqstA2DIMbxocO64rvytmcUeIwu8Mi5uq87KQP5AQMSMYb59Inbd+dmVfx5cJo3fRS4/5s3/Hg==-MIIEPjCCAiagAwIBAgIBBTANBgkqhkiG9w0BAQsFADAYMRYwFAYDVQQDDA1KZXRQcm9maWxlIENBMB4XDTE1MTEwMjA4MjE0OFoXDTE4MTEwMTA4MjE0OFowETEPMA0GA1UEAwwGcHJvZDN5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxcQkq+zdxlR2mmRYBPzGbUNdMN6OaXiXzxIWtMEkrJMO/5oUfQJbLLuMSMK0QHFmaI37WShyxZcfRCidwXjot4zmNBKnlyHodDij/78TmVqFl8nOeD5+07B8VEaIu7c3E1N+e1doC6wht4I4+IEmtsPAdoaj5WCQVQbrI8KeT8M9VcBIWX7fD0fhexfg3ZRt0xqwMcXGNp3DdJHiO0rCdU+Itv7EmtnSVq9jBG1usMSFvMowR25mju2JcPFp1+I4ZI+FqgR8gyG8oiNDyNEoAbsR3lOpI7grUYSvkB/xVy/VoklPCK2h0f0GJxFjnye8NT1PAywoyl7RmiAVRE/EKwIDAQABo4GZMIGWMAkGA1UdEwQCMAAwHQYDVR0OBBYEFGEpG9oZGcfLMGNBkY7SgHiMGgTcMEgGA1UdIwRBMD+AFKOetkhnQhI2Qb1t4Lm0oFKLl/GzoRykGjAYMRYwFAYDVQQDDA1KZXRQcm9maWxlIENBggkA0myxg7KDeeEwEwYDVR0lBAwwCgYIKwYBBQUHAwEwCwYDVR0PBAQDAgWgMA0GCSqGSIb3DQEBCwUAA4ICAQC9WZuYgQedSuOc5TOUSrRigMw4/+wuC5EtZBfvdl4HT/8vzMW/oUlIP4YCvA0XKyBaCJ2iX+ZCDKoPfiYXiaSiH+HxAPV6J79vvouxKrWg2XV6ShFtPLP+0gPdGq3x9R3+kJbmAm8w+FOdlWqAfJrLvpzMGNeDU14YGXiZ9bVzmIQbwrBA+c/F4tlK/DV07dsNExihqFoibnqDiVNTGombaU2dDup2gwKdL81ua8EIcGNExHe82kjF4zwfadHk3bQVvbfdAwxcDy4xBjs3L4raPLU3yenSzr/OEur1+jfOxnQSmEcMXKXgrAQ9U55gwjcOFKrgOxEdek/Sk1VfOjvS+nuM4eyEruFMfaZHzoQiuw4IqgGc45ohFH0UUyjYcuFxxDSU9lMCv8qdHKm+wnPRb0l9l5vXsCBDuhAGYD6ss+Ga+aDY6f/qXZuUCEUOH3QUNbbCUlviSz6+GiRnt1kA9N2Qachl+2yBfaqUqr8h7Z2gsx5LcIf5kYNsqJ0GavXTVyWh7PYiKX4bs354ZQLUwwa/cG++2+wNWP+HtBhVxMRNTdVhSm38AknZlD+PTAsWGu9GyLmhti2EnVwGybSD2Dxmhxk3IPCkhKAK+pl0eWYGZWG3tJ9mZ7SowcXLWDFAk0lRJnKGFMTggrWjV8GYpw5bq23VmIqqDLgkNzuoog==


