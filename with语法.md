# with语法

[相关资料](http://www.jb51.net/article/84065.htm)
```html
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>标题</title>
</head>
<body>
	<h1>标题</h1>
	<hr />
	
</body>
<script type="text/javascript">
	// with 语法减少对象的书写
	// 如果需要大量调用某个对象的方法或者属性的时候，就会多次使用同样的 obj.attr 语句，这时就可以使用with语法，
        // 把所有以 obj对象为参考对象的语句放到With 语句块中，从而达到减少语句量的目的。

	// http://www.jb51.net/article/84065.htm
	var json = {name:'caoyang', age:12, getName:function(){
		console.log( this.name );
	}};
	name = 'window-name';
	with(json){    
		getName();
		console.log( name );
	}
	console.log( name );

	with(json){
		var number = 12;
		console.log( number );
		var sex = 'm';
	}
	console.log( number );
	console.log( sex );

	// 刚才研究了一下，with的用法是这样的：with(object) {}，在大括号里面，
        // 可以引用object的属性而不用使用object.attr这种形式。你的代码中，with里面接受了一个对象，
        // 只不过这个对象是函数，函数有length属性，代表形参的个数，所以上面返回的值是2

	with (function(x, undefined){}) console.log(length);

</script>
</html>
```